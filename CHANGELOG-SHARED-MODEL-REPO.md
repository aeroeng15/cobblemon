
### Additions
- Added support for NPCs to use Pokémon models and vice versa. This will require changes in addons that add fossil types due to naming conflicts between fossils and the Pokémon species.


### Developer
- Removed all VaryingModelRepository subclasses into the parent class. 